package com.shchipunov.workshop.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * @author Shchipunov Stanislav
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldShowIndexPage() throws Exception {
		mockMvc
			.perform(get("/").secure(true))
			.andExpect(status().isOk())
			.andExpect(view().name("indexPage"));
	}

	@Test
	@WithUserDetails("user")
	public void shouldShowUserPage() throws Exception {
		mockMvc
			.perform(get("/dashboard").secure(true))
			.andExpect(status().isOk())
			.andExpect(view().name("userPage"));
	}

	@Test
	@WithUserDetails("administrator")
	public void shouldShowAdministratorPage() throws Exception {
		mockMvc
			.perform(get("/administrator").secure(true))
			.andExpect(status().isOk())
			.andExpect(view().name("administratorPage"));
	}

	@Test
	public void shouldShow302() throws Exception {
		mockMvc
			.perform(get("/dashboard").secure(true))
			.andExpect(status().is3xxRedirection())
			.andExpect(header().string(HttpHeaders.LOCATION, "http://localhost/logIn"));
	}
}
