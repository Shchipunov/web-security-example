package com.shchipunov.workshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shchipunov.workshop.domain.Account;

/**
 * Repository to access {@link Account} instances
 * 
 * @author Shchipunov Stanislav
 */
public interface AccountRepository extends JpaRepository<Account, String> {
	
}
