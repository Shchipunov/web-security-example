package com.shchipunov.workshop;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import com.shchipunov.workshop.repository.AccountRepository;

/**
 * Security configuration
 *
 * @author Shchipunov Stanislav
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * Customized user details service
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(userDetailsService());
	}

	@Override
	public void configure(WebSecurity webSecurity) {
		webSecurity.ignoring()
				.antMatchers("/resources/static/**");
	}

	/**
	 * Main security configuration. There is no need to specify some of them
	 * explicitly - as an example "headers". It's done just to show how it can
	 * be configured.
	 */
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.authorizeRequests()
				.antMatchers("/administrator/**").hasRole("ADMINISTRATOR")
				.antMatchers("/dashboard").authenticated()
				.anyRequest().permitAll()
			.and().requiresChannel()
				.antMatchers("/**").requiresSecure()
			.and().formLogin()
				.loginPage("/logIn")
				.usernameParameter("username")
				.passwordParameter("password")
				.loginProcessingUrl("/authenticate")
				.successHandler(authenticationSuccessHandler())
				.failureUrl("/logIn?error=true")
				.permitAll()
			.and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logOut"))
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.logoutSuccessUrl("/logIn")
			.and().sessionManagement()
				.sessionFixation()
				.changeSessionId()
				.maximumSessions(1)
				.maxSessionsPreventsLogin(true)
				.sessionRegistry(sessionRegistry())
				.expiredUrl("/logIn?expired")
			.and().and().csrf()
			.and().headers()
					.cacheControl()
	        	.and()
	        		.contentTypeOptions()
	        	.and()
	        		.frameOptions()
	        	.and()
	        		.httpStrictTransportSecurity()
	        	.and()
	        		.xssProtection();
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return (username) -> Optional.ofNullable(accountRepository.findOne(username))
				.orElseThrow(() -> new UsernameNotFoundException("Could not find: " + username));
	}

	/**
	 * Customized authentication success handler (the main goal is to redirect
	 * to the appropriate page after successful authentication)
	 */
	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler() {
		return new AuthenticationSuccessHandler() {

			@Override
			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
					Authentication authentication) throws IOException, ServletException {
				Set<String> authorities = authentication.getAuthorities().stream()
						.map(GrantedAuthority::getAuthority)
						.collect(Collectors.toSet());
				if (authorities.contains("ROLE_USER")) {
					response.sendRedirect("/dashboard");
				} else if (authorities.contains("ROLE_ADMINISTRATOR")) {
					response.sendRedirect("/administrator");
				}
			}
		};
	}

	@Bean
	public static ServletListenerRegistrationBean<HttpSessionEventPublisher> servletListenerRegistrationBean() {
		return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
	}

	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}

	/**
	 * Provides the new dialect (used in views)
	 */
	@Bean
	public SpringSecurityDialect springSecurityDialect() {
		return new SpringSecurityDialect();
	}

	/**
	 * Annotation-based method security
	 */
	@Configuration
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	public static class MethodSecurityConfiguration extends GlobalMethodSecurityConfiguration {

	}
}
