package com.shchipunov.workshop.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Basic controller
 * 
 * @author Shchipunov Stanislav
 */
@Controller
public class ApplicationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String showIndexPage() {
		LOGGER.debug("Welcome to WEB-security example!");
		return "indexPage";
	}

	/**
	 * Shows log in page
	 * 
	 * @return
	 */
	@RequestMapping(value = "/logIn", method = RequestMethod.GET)
	public String showLogInPage(Principal principal) {
		return principal != null ? "redirect:/dashboard" : "logInPage";
	}

	/**
	 * Shows user page
	 * 
	 * @return
	 */
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = { "/dashboard" }, method = RequestMethod.GET)
	public String showUserPage() {
		return "userPage";
	}

	/**
	 * Shows administrator page
	 * 
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
	@RequestMapping(value = { "/administrator" }, method = RequestMethod.GET)
	public String showAdministratorPage() {
		return "administratorPage";
	}
}
