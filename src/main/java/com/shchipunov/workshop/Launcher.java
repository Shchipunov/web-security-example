package com.shchipunov.workshop;

import java.io.IOException;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

/**
 * @author Shchipunov Stanislav
 */
@SpringBootApplication
public class Launcher {

	public static void main(String[] arguments) {
		SpringApplication.run(Launcher.class, arguments);
	}
	
	/**
	 * Customizes secured connections through HTTPS. It's an alternative of
	 * configuration that can be specified in the properties file (see
	 * application.properties file).
	 * 
	 * @param resource
	 *            indicates a location of the key-store
	 * @param password
	 *            contains a password from the key-store
	 * @param alias
	 *            contains an alias value
	 */
	@Bean
	EmbeddedServletContainerCustomizer containerCustomizer(@Value("${key-store}") Resource resource,
			@Value("${key-store-password}") String password, @Value("${key-alias}") String alias) {
		return (ConfigurableEmbeddedServletContainer container) -> {
			if (container instanceof TomcatEmbeddedServletContainerFactory) {
				Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
				Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
				try {
					String pathToFile = resource.getFile().getAbsolutePath();
					connector.setScheme("https");
					connector.setSecure(true);
					connector.setPort(8443);
					protocol.setSSLEnabled(true);
					protocol.setKeystoreFile(pathToFile);
					protocol.setKeystorePass(password);
					protocol.setKeyAlias(alias);
				} catch (IOException exception) {
					throw new IllegalStateException("Can't access key store!", exception);
				}
				TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) container;
				containerFactory.addAdditionalTomcatConnectors(connector);
			}
		};
	}
}
