# WEB-security example #

### Project description ###
The project demonstrates:

* HTTPS
* Authentication / authorization
* Session fixation protection
* Cross-site scripting (XSS) attacks protection
* Cross-site request forgery attacks protection
* Clickjacking attacks protection

The project uses:

* Spring Boot
* Spring (MVC)
* Spring Security
* Spring Data JPA
* Thymeleaf